(defproject open_space "0.1.0"
  :description "Open Space: A Community Project Space"
  :url ""
  :license {:name "MIT License"
            :url "http://opensource.org/licenses/MIT"}

  :dependencies [[org.clojure/clojure "1.5.1"]
                 [ring/ring-jetty-adapter "1.2.0"]
                 [org.clojure/java.jdbc "0.2.3"]
                 [org.postgresql/postgresql "9.2-1003-jdbc4"]
                 [compojure "1.1.5"]
                 [hiccup "1.0.4"]]
  :plugins [[clj-sql-up "0.2.0"]
            [lein-ring "0.8.5"]]
  :clj-sql-up {:database "jdbc:postgresql://127.0.0.1:5432/open_space?slackz"
               :deps [[org.postgresql/postgresql "9.2-1003-jdbc4"]]}
  :main open-space.core
  :ring {:handler open-space.core/app})

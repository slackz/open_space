(ns open-space.core
  (:use compojure.core)
  (:require [ring.adapter.jetty        :as jetty]
            [compojure.handler         :as handler]
            [compojure.route           :as route]
            [open-space.views.static   :as static]))

(defroutes app-routes
  (GET "/"          [] (static/home))
  (GET "/darkroom"  [] (static/darkroom))
  (GET "/workspace" [] (static/workspace))
  (GET "/cafe"      [] (static/cafe))
  (GET "/about"     [] (static/about))
  (GET "/contact"   [] (static/contact))
  (route/files "/")
  (route/not-found "Page not found"))

(def app
  (handler/site app-routes))

(defn -main []
  (jetty/run-jetty #'app-routes {:port 8080}))
